'use strict';

var usernamePage = document.querySelector('#username-page');
var chatPage = document.querySelector('#chat-page');
var usernameForm = document.querySelector('#usernameForm');
var messageForm = document.querySelector('#messageForm');
var messageInput = document.querySelector('#message');
var messageArea = document.querySelector('#messageArea');
var connectingElement = document.querySelector('.connecting');
var typingElemt = document.querySelector('#typing');

var stompClient = null;
var username = null;
var textValue = null;

var colors = [
    '#2196F3', '#32c787', '#00BCD4', '#ff5652',
    '#ffc107', '#ff85af', '#FF9800', '#39bbb0'
];

function connect(event) {
    username = document.querySelector('#name').value.trim();

    if (username) {
        usernamePage.classList.add('hidden');
        chatPage.classList.remove('hidden');

        var socket = new SockJS('/ws');

        stompClient = Stomp.over(socket);

        stompClient.connect({}, onConnected, onError);
    }
    event.preventDefault();
}


function onConnected() {
    // Subscribe to the Public Topic
    stompClient.subscribe('/topic/public', onMessageReceived);

    // Tell your username to the server
    stompClient.send("/app/chat.addUser",
        {},
        JSON.stringify({ sender: username, type: 'JOIN' })
    )

    connectingElement.classList.add('hidden');
}

function userTyping() {
    setTimeout(() => {
        if (textValue != messageInput.value.trim()) {
            textValue = messageInput.value.trim();
            stompClient.send("/app/chat.typing", {},
                JSON.stringify({ sender: username, type: 'TYPING', content: 'start' }));
        } else {
            stompClient.send("/app/chat.typing", {},
                JSON.stringify({ sender: username, type: 'TYPING', content: 'stop' }));
        }
    }, 1000);
}


function onError(error) {
    connectingElement.textContent = 'Could not connect to WebSocket server. Please refresh this page to try again!';
    connectingElement.style.color = 'red';
}


function sendMessage(event) {
    var messageContent = messageInput.value.trim();
    textValue = null;
    if (messageContent && stompClient) {
        var chatMessage = {
            sender: username,
            content: messageInput.value,
            type: 'CHAT'
        };

        stompClient.send("/app/chat.sendMessage", {}, JSON.stringify(chatMessage));
        messageInput.value = '';
    }
    event.preventDefault();
}


function onMessageReceived(payload) {
    var message = JSON.parse(payload.body);

    var messageElement = document.createElement('li');

    if (message.type === 'JOIN') {
        typingElemt.setAttribute('style', 'visibility: hidden;');
        messageElement.classList.add('event-message');
        message.content = message.sender + ' joined!';
    } else if (message.type === 'LEAVE') {
        typingElemt.setAttribute('style', 'visibility: hidden;');
        messageElement.classList.add('event-message');
        message.content = message.sender + ' left!';
    } else if (message.type === 'TYPING' && message.sender != username) {
        if (message.content == 'start') {
        typingElemt.innerHTML = message.sender + ' is typing...';
            console.log(message.sender + ' is typing...');
            typingElemt.setAttribute('style', 'visibility: visible;text-align: center;');
        } else {
            typingElemt.setAttribute('style', 'visibility: hidden;');
        }
    } else {
        typingElemt.setAttribute('style', 'visibility: hidden;');
        messageElement.classList.add('chat-message');
        var avatarElement = document.createElement('i');
        var avatarText = document.createTextNode(message.sender[0]);
        avatarElement.appendChild(avatarText);
        avatarElement.style['background-color'] = getAvatarColor(message.sender);

        messageElement.appendChild(avatarElement);

        var usernameElement = document.createElement('span');
        var usernameText = document.createTextNode(message.sender);
        usernameElement.appendChild(usernameText);
        messageElement.appendChild(usernameElement);
    }

    if (message.type !== 'TYPING') {
        var textElement = document.createElement('p');
        if (message.content.startsWith('file>')) {
            let msg = message.content;
            let fileName = msg.substring(msg.indexOf('>') + 1, msg.lastIndexOf('>'));
            let fileUrl = msg.substring(msg.lastIndexOf('>') + 1);
            console.log(fileUrl);
            console.log(fileName);
            var aTag = document.createElement('a');
            aTag.setAttribute('href', fileUrl);
            aTag.setAttribute('target', '_blank');
            var imgTag = document.createElement('img');
            if (strEndsWith(fileUrl.trim(), 'pdf')) {
                console.log("pdf");
                imgTag.setAttribute('height', 100);
                imgTag.setAttribute('width', 100);
                imgTag.setAttribute('src', "../img/pdf_thumbnail.png");
            } else {
                imgTag.setAttribute('height', 100);
                imgTag.setAttribute('width', 150);
                imgTag.setAttribute('src', fileUrl);
            }
            var divTag = document.createElement('div');
            divTag.innerHTML = fileName;
            aTag.appendChild(imgTag);
            aTag.appendChild(divTag);
            textElement.appendChild(aTag);
        } else {
            var messageText = document.createTextNode(message.content);
            textElement.appendChild(messageText);
        }
        messageElement.appendChild(textElement);

        messageArea.appendChild(messageElement);
        messageArea.scrollTop = messageArea.scrollHeight;
    }
}


function strEndsWith(str, suffix) {
    return str.match(suffix + "$") == suffix;
}


function getAvatarColor(messageSender) {
    var hash = 0;
    for (var i = 0; i < messageSender.length; i++) {
        hash = 31 * hash + messageSender.charCodeAt(i);
    }

    var index = Math.abs(hash % colors.length);
    return colors[index];
}

function uploadFile(event) {
    textValue = null;
    const files = document.querySelector('[type=file]').files;
    const formData = new FormData();
    formData.append("file", files[0], files[0].name);
    console.log("form data", formData);
    var fileMessage = {
        sender: username,
        type: 'CHAT'
    };
    formData.append("fileMessage", JSON.stringify(fileMessage));
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "/sendFile", true);
    xhttp.send(formData);
}

usernameForm.addEventListener('submit', connect, true)
messageForm.addEventListener('submit', sendMessage, true)

messageInput.addEventListener('keyup', userTyping, true);

var uploadBtn = document.querySelector('.upload-btn');
uploadBtn.addEventListener('click', uploadFile, true);
