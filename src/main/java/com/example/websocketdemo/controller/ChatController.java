package com.example.websocketdemo.controller;

import com.example.websocketdemo.config.AmazonClient;
import com.example.websocketdemo.dto.ChatDTO;
import com.example.websocketdemo.model.ChatMessage;
import com.example.websocketdemo.model.FileMessage;
import com.example.websocketdemo.repository.ChatRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RestController
public class ChatController {

    @Autowired
    private AmazonClient amazonClient;

    @Autowired
    ChatRepository chatRepository;

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    @Autowired
    private HttpServletRequest request;

    @MessageMapping("/chat.sendMessage")
    @SendTo("/topic/public")
    public ChatMessage sendMessage(@Payload ChatMessage chatMessage, SimpMessageHeaderAccessor headerAccessor) {
        System.out.println(chatMessage);
        ChatDTO chatDTO = new ChatDTO();
        chatDTO.setSessionId(headerAccessor.getSessionId());
        chatDTO.setMessage(chatMessage.getContent());
        chatDTO.setFromUser(chatMessage.getSender());
        ChatDTO savedChatDTO = chatRepository.save(chatDTO);
        System.out.println(savedChatDTO.toString());
        return chatMessage;
    }

    @MessageMapping("/chat.typing")
    @SendTo("/topic/public")
    public ChatMessage userTyping(@Payload ChatMessage chatMessage, SimpMessageHeaderAccessor headerAccessor) {
        System.out.println(chatMessage.toString());
        return chatMessage;
    }

    @MessageMapping("/chat.addUser")
    @SendTo("/topic/public")
    public ChatMessage addUser(@Payload ChatMessage chatMessage,
                               SimpMessageHeaderAccessor headerAccessor) {
        // Add username in web socket session
        headerAccessor.getSessionAttributes().put("username", chatMessage.getSender());
        ChatDTO chatDTO = new ChatDTO();
        chatDTO.setSessionId(headerAccessor.getSessionId());
        chatDTO.setMessage("JOIN");
        chatDTO.setFromUser(chatMessage.getSender());
        ChatDTO savedChatDTO = chatRepository.save(chatDTO);
        System.out.println(savedChatDTO.toString());
        return chatMessage;
    }

    @RequestMapping(value = "/sendFile", method = RequestMethod.POST)
    @SendTo("/topic/public")
    public ChatMessage sendFile(@RequestPart("fileMessage") String msgString, @RequestPart("file") MultipartFile file, HttpSession httpSession) {
        ObjectMapper mapper = new ObjectMapper();
        FileMessage fileMessage = null;
        try {
            fileMessage = mapper.readValue(msgString, FileMessage.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String url = amazonClient.uploadFile(file);
        ChatMessage chatMessage = new ChatMessage();
        chatMessage.setType(ChatMessage.MessageType.CHAT);
        chatMessage.setSender(fileMessage.getSender());
        chatMessage.setContent("file>" + file.getOriginalFilename() + ">" + url);
        ChatDTO chatDTO = new ChatDTO();
        chatDTO.setSessionId(httpSession.getId());
        chatDTO.setMessage(chatMessage.getContent());
        chatDTO.setFromUser(chatMessage.getSender());
        ChatDTO savedChatDTO = chatRepository.save(chatDTO);
        System.out.println(savedChatDTO.toString());
        simpMessagingTemplate.convertAndSend("/topic/public", chatMessage);
        return chatMessage;
    }

}
