package com.example.websocketdemo.repository;

import com.example.websocketdemo.dto.ChatDTO;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

@Repository
public interface ChatRepository extends CrudRepository<ChatDTO, Serializable> {
}
