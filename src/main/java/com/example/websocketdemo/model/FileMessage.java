package com.example.websocketdemo.model;

import org.springframework.web.multipart.MultipartFile;

public class FileMessage {
    private String type;
    private String sender;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }
}
