package com.example.websocketdemo.dto;

import com.datastax.driver.core.DataType;
import com.example.websocketdemo.model.ChatMessage;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.CassandraType;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Table("chat_messages")
public class ChatDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @PrimaryKeyColumn(name = "id", ordinal = 0, type = PrimaryKeyType.PARTITIONED)
    @Column("id")
    @CassandraType(type = DataType.Name.UUID)
    private UUID id;

    @Column("session_id")
    private String sessionId;

    @Column("from_user")
    private String fromUser;

    @Column("message")
    private String message;

    @Column("created_time")
    private Date createdTime = new Date();

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getFromUser() {
        return fromUser;
    }

    public void setFromUser(String fromUser) {
        this.fromUser = fromUser;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public ChatDTO() {
        id = UUID.randomUUID();
    }

    public ChatDTO(UUID id, String sessionId, String message, String fromUser, Date createdTime) {
        this.id = id;
        this.sessionId = sessionId;
        this.message = message;
        this.fromUser = fromUser;
        this.createdTime = createdTime;
    }

    @Override
    public String toString() {
        return "ChatDTO{" +
                "id='" + id + '\'' +
                ", sessionId='" + sessionId + '\'' +
                ", fromUser='" + fromUser + '\'' +
                ", message='" + message + '\'' +
                ", createdTime=" + createdTime +
                '}';
    }
}
